# Selenium-Test

Este projeto é um presente à comunidade dentro da disciplina de MAC413/5714 - Tópicos Avançados de Programação Orientada a Objetos ministrada pelo Prof Fabio Kon do IME/USP.

Além dos diferentes acabouços visto em aula, apresentamos o Selenium que auxilia na automação de teste para processos Web.

## Pre-requisitos
Instalar o Visual Studio neste [link](https://visualstudio.microsoft.com/pt-br/downloads/).


## Configurações
Esta seção auxiliará nas diferentes configuações após a instalação do Visual Studio.

1. Abrir o Visual Studio e criar um projeto na aba `Create New Project`.

![Criação de projeto](images/create-project.png "image_tooltip")

Figura 1: Criação de projeto.

2. Procurar NUnit conforme a imagem:

![Tipo de projeto](images/nunit-project.png "image_tooltip")

Figura 2: Tipo de projeto.

3. Nas configurações, preencher o nome do Projeto, caminho e o nome da solução. Finalizar clicando no botão `Next` e depois `Create`.


![Configurações do projeto](images/project-name.png "image_tooltip")

Figura 3: Configurações do projeto.


![Configurações adicionais do projeto](images/project-config.png "image_tooltip")

Figura 4: Configurações adicionais do projeto.

4. Percebe-se que o Visual Studio já criou uma classe de Test e já configurou o ambiente de teste.

![Ambiente de teste](images/project-init.png "image_tooltip")

Figura 5: Ambiente de teste.

5. Instalar a biblioteca `Selenium Webdriver` no Nuget, gerenciador de pacote do Visual Studio.

![Instalação do Selenium Webdriver](images/instal-lib-steps.png "image_tooltip")

Figura 6: Instalação do pacote `Selenium Webdriver`.


![Instalação do Selenium Webdriver](images/find-libs.png "image_tooltip")

Figura 7: Instalação do pacote `Selenium Webdriver`.


6. Criar uma pasta para armazenar o driver do navegador.

![Criação da pasta de drivers](images/add-driver-folders.png "image_tooltip")

Figura 8: Criação da pasta de drivers.

7. Neste tutorial iremos trbalhar com o Chrome, porém os passos são semelhante para outros navegadores. Basta baixar o driver correspondente à versão do navegador instalada na máquina.

8. Com isso, deve-se baixar no [link](https://chromedriver.chromium.org/downloads) o driver do chrome de acordo com a versão do Chrome instalada no computador. 

9. Após isso, extrair o chromedriver.exe para a pasta drivers\ criada em 6.


## Teste com Selenium

Após realizar a instalação do visual Studio na seção anterior e as diferentes configurações, vamos iniciar os testes usando o Selenium.

1. Colar dentro da função `SeleniumTest.cs`:

```c#
//Inside SeleniumTest.cs

using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.ObjectModel;
using System.IO;

namespace SeleniumCsharp
{
    public class Tests
    {
        IWebDriver driver;

        [OneTimeSetUp]
        public void Setup()
        {            
            string path = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;        
            driver = new ChromeDriver(path+@"\drivers\");
        }

        [Test]
        public void verifyLogo()
        {
            driver.Navigate().GoToUrl("https://www.browserstack.com/");
            Assert.IsTrue(driver.FindElement(By.Id("logo")).Displayed);
        }

        [Test]
        public void verifyMenuItemcount()
        {
            ReadOnlyCollection<IWebElement> menuItem = driver.FindElements(By.XPath("//ul[contains(@class,'horizontal-list product-menu')]/li"));
            Assert.AreEqual(menuItem.Count, 4);
        }

        [Test]
        public void verifyPricingPage()
        {
            driver.Navigate().GoToUrl("https://browserstack.com/pricing");
            IWebElement contactUsPageHeader = driver.FindElement(By.TagName("h1"));
            Assert.IsTrue(contactUsPageHeader.Text.Contains("Replace your device lab and VMs with any of these plans"));
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            driver.Quit();

        }
    }
}
```

2. Executar os testes:

- Abrir a janela de teste,

![Janela de Teste](images/test-1.png "image_tooltip")

Figura 9: Janela de Teste.

- Executar os testes,

![Execução de Teste](images/test-2.png "image_tooltip")

Figura 10: Execução de Teste.

- Resultado dos testes,

![Resultado dos testes](images/test-3.png "image_tooltip")

Figura 11: Resultado dos testes